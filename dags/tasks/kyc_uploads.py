from google.cloud import storage
import json
import datetime
from os import sys
import os
import os.path

with open("/home/mgssalim/code/preprocessing_kyc/kyc_config.json", "r") as read_file:
    CONFIG = json.load(read_file)['kyc_conffig']

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = CONFIG['service_account']
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    print('success')
    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}/{}.'.format(
        source_file_name,
        bucket,
        destination_blob_name))

def main():
    downloaded_voices = os.listdir(CONFIG['dataset_path'])
    i = 0
    for folders in downloaded_voices:
        
        # folders = downloaded_voices[1]
        # for i in folders:
        #     print(i)
        # # files = os.listdir(CONFIG['dataset_path']+folders)
        # print(files)
        # # source_file_name = '{dataset_path}{folders}/{files}'.format(
        # #         dataset_path=CONFIG['dataset_path'],files=files,folders=folders)
        
        # # if(os.path.exists(source_file_name)):
        # #     bucket_name = CONFIG['bucket']
            
        # #     destination_blob_name = '{target_dir}{folders}/{files}'.format(
        # #         target_dir=CONFIG['target_dir'],folders=folders,files=files)
            
        # #     upload_blob(bucket_name,source_file_name,destination_blob_name)
        # #     print('--->> success upload to {target_dir}{folders}/{files}'.format(
        # #         target_dir=CONFIG['target_dir'],folders=folders,files=files))
        # #     os.remove(source_file_name)
        # #     print('success delete')
        # # else:
        # #     print('No Data !!!')
        # print(i)
        # i+=1
    #fraud-middleware-endpoints/voice-data-valid/06_07_2019/62087780572014_06_07_2019_1.wav
        for files in os.listdir(CONFIG['dataset_path']+folders):
            source_file_name = '{dataset_path}{folders}/{files}'.format(
            dataset_path=CONFIG['dataset_path'],files=files,folders=folders)
            if(os.path.exists(source_file_name)):
                bucket_name = CONFIG['bucket']
                
                destination_blob_name = '{target_dir}{folders}/{files}'.format(
                    target_dir=CONFIG['target_dir'],folders=folders,files=files)
                
                upload_blob(bucket_name,source_file_name,destination_blob_name)
                print('--->> success upload to {target_dir}{folders}/{files}'.format(
                    target_dir=CONFIG['target_dir'],folders=folders,files=files))
                os.remove(source_file_name)
                print('success delete')
            else:
                print('No Data !!!')
    
if __name__ == '__main__':
    main()