from google.cloud import storage
from os.path import expanduser
import os
home = expanduser("~")
json_path 		= '{home}/code/Tunaiku/know_your_customers/json_file/'.format(home = home)


account = {
				'bucket'			: 'e-curve-197803',
				'target_dir'		: 'project_ear_json_files/',
				'dir'				: 'project_ear_json_files/',
				'service_account'	: '/home/user/airflow/dags/tasks/e-curve-197803-5f5904b0c034.json',
				'local_folder'		: '{home}/Documents/project_ear_beta/dataset/json_files/'.format(home = home)
				}

#set client
client = storage.Client.from_service_account_json(account['service_account'])
bucket = client.get_bucket(account['bucket'])
# blobs  = bucket.list_blobs(prefix = account['target_dir'])
local_folder = os.path.join(account['local_folder'], account['target_dir'])
i=0
for file in os.listdir(json_path):
	blob = bucket.blob('test {0}'+str(i))
	blob.upload_from_filename(json_path + file)
	i+=1


# for blob in blobs:
	# print(blob)
	# for file in os.listdir(json_path):
	# 	print('uploading {} into GCS'.format(file))
	# 	blob.upload_from_filename(json_path + file)
