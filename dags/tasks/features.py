'''
function to extracts features from given voices (train and testing)
consists of various preprocessing
	- remove silence
	- normalize for energy density
	- convert to frequency domain -> transform into spectrogram/image/matrix -> MFCC13_F
	- calculate delta frequency -> delta_MFCC
	- calculate double delta frequency -> double_delta_MFCC
	- combine/concatenate MFCC13_F, delta_MFCC, double_delta_MFCC

returns: matrix of extracted features [ :, 13] vector
'''

# import modules
import numpy as np
import numpy
import scipy.io.wavfile
from scipy.fftpack import dct

from sklearn import mixture
from sklearn.externals import joblib
import glob

import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
import librosa

import IPython.display as ipd
import librosa.display

import scipy.stats as st

import copy



#extract features function
def create_features(path):

    x, sr = librosa.load(path)
    
    # remove silence
    y = librosa.effects.split(x,top_db=30)
    l = []
    for i in y:
        l.append( x[i[0]:i[1]] )
    x = np.concatenate(l,axis=0)
    
    # normalize for energy density
    VTH_Multiplier = 0.05
    VTH_range=100
    energy = [ s**2 for s in x]
    Voiced_Threshold = VTH_Multiplier*np.mean(energy)
    clean_samples=[0]

    for sample_set in range(0,len(x)-VTH_range,VTH_range):
        sample_set_th = np.mean(energy[sample_set:sample_set+VTH_range])
        if sample_set_th>Voiced_Threshold:
            clean_samples.extend(x[sample_set:sample_set+VTH_range])

    x = np.array(clean_samples)
    
    # convert to frequency domain
    MFCC = librosa.feature.mfcc(y=x, sr=sr, n_mfcc=13)
    MFCC13_F = MFCC.T
    
    calc_deltas=False

    # calcualte delta frequency
    delta_MFCC = np.zeros(MFCC13_F.shape)
    for t in range(delta_MFCC.shape[1]):
        index_t_minus_one,index_t_plus_one=t-1,t+1

        if index_t_minus_one<0:    
            index_t_minus_one=0
        if index_t_plus_one>=delta_MFCC.shape[1]:
            index_t_plus_one=delta_MFCC.shape[1]-1

        delta_MFCC[:,t]=0.5*(MFCC13_F[:,index_t_plus_one]-MFCC13_F[:,index_t_minus_one] )

    # calcualte double delta frequency
    double_delta_MFCC = np.zeros(MFCC13_F.shape)
    for t in range(double_delta_MFCC.shape[1]):

        index_t_minus_one,index_t_plus_one, index_t_plus_two,index_t_minus_two=t-1,t+1,t+2,t-2

        if index_t_minus_one<0:
            index_t_minus_one=0
        if index_t_plus_one>=delta_MFCC.shape[1]:
            index_t_plus_one=delta_MFCC.shape[1]-1
        if index_t_minus_two<0:
            index_t_minus_two=0
        if index_t_plus_two>=delta_MFCC.shape[1]:
            index_t_plus_two=delta_MFCC.shape[1]-1

        double_delta_MFCC[:,t]=0.1*( 2*MFCC13_F[:,index_t_plus_two]+MFCC13_F[:,index_t_plus_one]
                                    -MFCC13_F[:,index_t_minus_one]-2*MFCC13_F[:,index_t_minus_two] )

    # combine into model input matrix
    Combined_MFCC_F = np.concatenate((MFCC13_F,delta_MFCC,double_delta_MFCC),axis=1)
    #print(Combined_MFCC_F.shape) #comment this line if you want no print
    
    return Combined_MFCC_F