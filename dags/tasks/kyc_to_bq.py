from google.cloud import bigquery
import os
from datetime import datetime
import json
import shutil

with open("/home/mgssalim/code/bitbucket_tunaiku/preprocessing_kyc/kyc_config.json", "r") as read_file:
    CONFIG = json.load(read_file)['kyc_conffig']

def load_data_to_bq():
    """
    loads or initalizes the job in BQ from firebase child
    :param df1: dataframe returned from the get_childs_from_db method
    :return: load job success notification
    """
    now = datetime.now()
    dates_nodash = now.strftime("%Y%m%d")
    dates = now.strftime("%d_%m_%y")
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = CONFIG['service_account']
    bigquery_client = bigquery.Client(CONFIG['project_id'])
    # print(bigquery_client)
    destination_dataset_ref = bigquery_client.dataset(CONFIG['destination_dataset'])
    destination_table_ref = destination_dataset_ref.table(CONFIG['destination_table'] + '$' + dates_nodash)
    job_config = bigquery.LoadJobConfig()
    job_config.create_disposition = bigquery.CreateDisposition.CREATE_IF_NEEDED
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    job_config.time_partitioning = bigquery.TimePartitioning(type_=bigquery.TimePartitioningType.DAY,
                                                                field="created")
    folders = '{folder}/{dates}'.format(folder=CONFIG['folder_json'],dates=dates)
    files = os.listdir(folders)
    for data in files:    
        with open(folders+'/'+data, 'rb') as f:
            #job = bigquery_client.load_table_from_file(f, destination_table_ref, job_config=job_config)
            #job.result()
            print('------>>> success loading data to {destination_table_ref}'.format(destination_table_ref=destination_table_ref))
    
    shutil.rmtree(folders)
    print('folder json file ==>> {folders} has removed!'.format(folders=folders))
    
    folder_voice = '{folder}{dates}'.format(folder=CONFIG['dataset_path'],dates=dates)
    shutil.rmtree(folder_voice)
    print('folder voice file ==>> {folder_voice} has removed!'.format(folder_voice=folder_voice))
    
if __name__ == '__main__':
    load_data_to_bq()