'''
import modules
'''
import os 
import pickle
import ndjson
from sklearn import mixture
import pandas as pd
import json

from features import create_features #python scripts
from datetime import date

with open("/home/mgssalim/code/bitbucket_tunaiku/preprocessing_kyc/kyc_config.json", "r") as read_file:
    CONFIG = json.load(read_file)['kyc_conffig']

'''
creating trained GMM model file .gmm
'''
def populate_and_train():
	'''
	hyperparameters
	'''
	today = date.today()
	dates = today.strftime("%d_%m_%Y")
	n_mixtures = 32
	max_iterations = 128
	local_folder = CONFIG['dataset_path']+dates

	'''
	train model
	'''
	for files in os.listdir(local_folder):
	
		'''
		populate features to be stored in bigQuery
		'''
		features = create_features(local_folder + '/' + files)[ :None]
		var_features = [i for i in features]
		'''
		crate GMM model from given sample file
		'''
		gmm_model = mixture.GaussianMixture(n_components = n_mixtures, 
											covariance_type = 'diag',
											max_iter = max_iterations)

		'''
		train
		'''
		gmm_model.fit(features)

		
		'''
		dumping trained model to store_path
		'''
		tmp_filename = files.split('.')[0]
		pickle_file = tmp_filename + '.gmm'
		if not os.path.exists(CONFIG['stored_model_path'] + '/' + dates):
			os.makedirs(CONFIG['stored_model_path'] + '/' + dates)
		pickle.dump(gmm_model, open(CONFIG['stored_model_path'] + '/' + dates + '/' + pickle_file, 'wb'))
		print('succes save models to {}'.format(CONFIG['stored_model_path']))
	
		'''
		write into .json files first
		'''

		json_data = [{
						'id': tmp_filename,
						'created': today.strftime("%Y-%m-%d"),
						'score_samples': str(var_features)

					}]
  
		if not os.path.exists(CONFIG['folder_json'] + '/' + dates):
			os.makedirs(CONFIG['folder_json'] + '/' + dates)
		with open(CONFIG['folder_json'] + '/' + dates + '/' + tmp_filename + '.json', 'w') as curr_file:
			ndjson.dump(json_data, curr_file)
			print('succes save feature to {}'.format(CONFIG['folder_json']))
	return

if __name__ == '__main__':
    populate_and_train()