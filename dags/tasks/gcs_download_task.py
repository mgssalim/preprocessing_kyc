#import modules 
import os
import logging
import shutil
import argparse
import json
from datetime import date

#import google storage libraries
from google.cloud import storage

from os.path import expanduser

with open("/home/mgssalim/code/preprocessing_kyc/kyc_config.json", "r") as read_file:
    CONFIG = json.load(read_file)['kyc_conffig']

today = date.today()
dates = today.strftime("%d_%m_%Y")

def download():
	#set client
	client = storage.Client.from_service_account_json(CONFIG['service_account'])
	bucket = client.get_bucket(CONFIG['bucket'])
	bucket_folder = CONFIG['target_dir']
	local_folder = CONFIG['dataset_path']+dates
	blobs  = bucket.list_blobs(prefix = bucket_folder+dates)
	'''
	get all downloaded voices list to avoid downloading the same files again
	'''
	
	downloaded_voices = [i.split('.')[0] for i in os.listdir(CONFIG['stored_model_path']+'/'+dates)]
	for blob in blobs:
		#avoid downloading the same files again
		file_name = blob.name.split('/')[-1]
		files = file_name.split('.')[0]
		if(files in downloaded_voices):
			print('folder {} has been there!'.format(local_folder))
			if(file_name in local_folder+'/'+file_name):
				print('file {} has been download!'.format(file_name))
				continue
			else:
				destination_uri = '{}/{}'.format(local_folder, blob.name)
				print('---> downloading: {}'.format(destination_uri))
				blob.download_to_filename(destination_uri)
		else:
			if not os.path.exists(local_folder):
				os.makedirs(local_folder)
			destination_uri = '{}/{}'.format(local_folder, file_name)
			print('---> downloading: {}'.format(destination_uri))
			blob.download_to_filename(destination_uri)

if __name__ == '__main__':
    download()