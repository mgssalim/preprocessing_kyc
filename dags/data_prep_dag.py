from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
import datetime as dt

#import python scripts for task
from tasks import gcs_download_task
from tasks import train_task
from tasks import kyc_to_bq
from tasks import delete_data_features
from tasks import delete_data_voice

default_args = {
	'owner': 'airflow',
	'start_date': dt.datetime(2019, 9, 25, 10, 00, 00),
	'concruency': 1,
	'retries': 0,
	'max_active_runs': 1
}

with DAG('data_prep_dag',
		 default_args = default_args,
		 schedule_interval = "@once" #run only once for developing
	) as dag:

	opr_gcs_download 		= PythonOperator(task_id = 'download_gcs',
	 								  		 python_callable = gcs_download_task.download)

	opr_populate_and_train	= PythonOperator(task_id = 'train_model',
											 python_callable = train_task.populate_and_train)

	opr_to_bq				= PythonOperator(task_id = 'load_to_bq',
                               				 python_callable = kyc_to_bq.load_data_to_bq)

opr_gcs_download >> opr_populate_and_train >> opr_to_bq